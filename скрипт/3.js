var filter_country_el = document.getElementById('filter_country');
var filter_time_el = document.getElementById('filter_time');
var filter_soys_el = document.getElementById('filter_soys');
var items_el = document.getElementById('items');

var filter = function() {
    var items = items_el.getElementsByClassName('item');
    for (var i = 0; i < items.length; i++) {
        if ((filter_country_el.value == 'all' || filter_country_el.value == items[i].dataset.country) &&
            (filter_time_el.value == 'all' || filter_time_el.value == items[i].dataset.time)&&
			(filter_soys_el.value == 'all' || filter_soys_el.value == items[i].dataset.soys)) {
            items[i].style.display = 'block';
        } else {
            items[i].style.display = 'none';
        }
    }
};

filter_country_el.addEventListener("change", filter);
filter_time_el.addEventListener("change", filter);
filter_soys_el.addEventListener("change", filter);